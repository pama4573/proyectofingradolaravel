<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    public function category() 
    {
        return $this->belongsTo(Category::class);
    }

    protected $primaryKey = 'id_product';
    
    protected $fillable = [
        'description', 'title', 'image', 'stock', 'price','size','color','author','id_category',
    ];

    public function scopeSearchProducts($query, $tipo, $buscar) {
        if ( ($tipo) && ($buscar) ) {
            return $query->where($tipo,'like',"%$buscar%");
        }
    }
    
    public function scopeSearchCategories($query,  $searchC) {
        if ( ($searchC) ) {
            return $query->where('id_category','like',"%$searchC%");
        }
    }

    public function scopeSearchPrice($query,  $priceSearch) {
        if ( ($priceSearch) ) {
            return $query->orderBy('price', $priceSearch);
        }
    }

    public function scopeSearchPriceLike($query, $searchP) {
        if ( ($searchP) ) {
            return $query->where('price','like',"%$searchP%");
        }
    }

}
