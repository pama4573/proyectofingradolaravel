<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function product()
    {
        return $this->hasMany(User::class);
    } 
    protected $fillable = [
        'name',
    ];
}
