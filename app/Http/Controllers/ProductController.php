<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Route;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\User;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $buscar = $request->get('searchProduct');

        $tipo = $request->get('type');
        $searchC = $request->get('searchC');
        $searchP = $request->get('searchP');

        $products = Product::searchproducts($buscar,$tipo)->searchcategories($searchC)->searchprice($searchP)->paginate(6);
        
        $categories = Category::all();
       
        return view('index', [
            'products'=>$products,
        'categories' => $categories,
        ]);
    } */

    public function index(Request $request)
    {
    $buscar = $request->get('searchProduct');

        $tipo = $request->get('type');
        $searchC = $request->get('searchC');
        $searchP = $request->get('searchP');
        $priceSearch = $request->get('priceSearch');

        $products = Product::searchproducts($buscar,$tipo)->searchcategories($searchC)->searchprice($priceSearch)->searchpricelike($searchP)->paginate(8);
        //$products = Product::orderBy('price', 'desc')->get()->paginate(8);
        $categories = Category::all();

        return view('index', [
            'products'=>$products,
            'categories' => $categories,
        ]);
    }

    public function listAllProducts()
    {
        $products = Product::paginate(10);
        $categories = Category::all();
        
        return view('user.listProductAuthor', [
            'products' => $products,
            'categories' => $categories,
        ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //$product = new Product();

        
        $this->validate($request, [
            'description' => 'required|min:8',
            'title' => 'required',
            'image' => 'max:2500', 
            'stock' => 'required',
            'price' => 'required',
            'size' => 'required',
            'color' => 'required',
            
            
            ]);
            //$imageName = time().'.'.$request->image->extension();  

           // $request->image->move(public_path('images'), $imageName);
            
           $file = $request->file('image');

           //obtenemos el nombre del archivo
           
           if($archivo = $file){
            $nombre = $file->getClientOriginalName();
            $archivo->move('img',$nombre);
            $product['description']=request('description');
            $product['title']=request('title');
            $product['image']=$nombre;
            $product['stock']=request('stock');
            $product['price']=request('price');
            $product['color']=request('color');
            $product['size']=request('size');
            $product['author']=request('author');
            $product['id_category']=request('id_category');
              
           }
           Product::create($product);
           //indicamos que queremos guardar un nuevo archivo en el disco local
         
          return redirect('/listProductAuthor');
     //així anirem de nou a llistar-ho tot
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateForm($id)
    {
      $product = Product::find($id);   //busco per la primary key que al nostre cas és l'id
      return view('products.create', ['product' => $product]);
    }
     
    // Actualitza la categoria a la base de dades
    public function update(Request $request)
    {
      $this->validate($request, [
        'id' => 'required|numeric|min:0',
        'description' => 'required|min:8',
        'title' => 'required',
        'image' => 'max:2500', 
        'stock' => 'required',
        'price' => 'required',
        'size' => 'required',
        'color' => 'required',
        'id_category' => 'required',
      ]);
     
      $id = $request->input('id'); 
     
      $product = Product::find($id);

    //  dd($product);//producto viejo
    
      //modificas con lo del formulario
      $product->id_product = $request->input('id'); 
      $product->description = $request->input('description'); 
      $product->title = $request->input('title'); 
      $product->image = $request->file('image'); 
      $product->stock = $request->input('stock'); 
      $product->price = $request->input('price'); 
      $product->size = $request->input('size'); 
      $product->color = $request->input('color'); 
      $product->id_category = $request->input('id_category'); 
   
      //  dd($product);//producto nuevo
    
      $file = $request->file('image');

           //obtenemos el nombre del archivo
           
           if($archivo = $file){
            $nombre = $file->getClientOriginalName();
            $archivo->move('img',$nombre);
            $product['image']=$nombre;
           }
           //indicamos que queremos guardar un nuevo archivo en el disco local
            $product->save();
     
      return redirect('/listProductAuthor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products=Product::findOrFail($id);
        $products->delete();
        return redirect('/listProductAuthor');
    }
    public function listProduct($id)
    {
        $product = Product::find($id);
        $products = Product::all();
        $categories = Category::all();
        
        return view('products.description', [
        'products' => $products,
        'categories' => $categories,
        ]);
    }
}