<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAllUsers()
    {
        //$usuarios = User::orderBy('created_at', 'desc')->get();
        $usuarios = User::all();
        $roles = Role::all();

        return view('admin.usersCRUD', [
            'usuarios' => $usuarios,
            'roles' => $roles,
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    // Mostra la vista formcat passant-li la categoria
    public function updateForm($id)
    {
    $usuario = User::find($id);   //busco per la primary key que al nostre cas és l'id
    return view('admin.modifyUser', ['usuario' => $usuario]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    $this->validate($request, [
        'name' => 'required|min:3',
        'email' => 'required|min:3',
        'address' => 'required|min:3',
    ]);
    
    $id = $request->input('id'); 
    
    $usuario = User::find($id);
    $usuario->id_user = $request->input('id'); 
    $usuario->name = $request->input('name'); 
    $usuario->email = $request->input('email');
    $usuario->address = $request->input('address'); 
    $usuario->id_role = $request->input('id_role'); 
    $usuario->save();
    
    return redirect('/usersCRUD');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = User::findOrFail($id);
        $usuarios->delete();
    
        return redirect('/usersCRUD');
    }
      // Mostra la vista formcat passant-li la categoria
      public function updateProfile($id)
      {
         // dd($id);
      $usuario = User::find($id);   //busco per la primary key que al nostre cas és l'id
      return view('user.editProfile', ['usuario' => $usuario]);
      }
      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function updateP(Request $request)
      {
      $this->validate($request, [
          'name' => 'required|min:3',
          'email' => 'required|min:3',
          'address' => 'required|min:3',
      ]);
      //$oldname = $request->get('name');
      $id = $request->input('id'); 
      
      $usuario = User::find($id);
      $usuario->id_user = $request->input('id'); 
      $usuario->name = $request->input('name'); 
      $usuario->email = $request->input('email');
      $usuario->address = $request->input('address'); 
      $usuario->id_role = $request->input('id_role'); 
      $usuario->save();
      $newname = $request->get('name');
      /*$users = User::updatename($newname,$oldname);
      return redirect('user.editProfile',[
        'users'=>$users]);
      }*/
      return redirect('/');
    }

}