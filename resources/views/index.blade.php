@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

<!-- jQuery full con ajax -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>

<!-- Bootstrap 4.5 js -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
    integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
</script>

<link rel="stylesheet" href="{{ asset('css/index.css') }}">

<!-- Page Content -->
<div class="container">

    <header class="jumbotron my-4">
        <h2 class="display-3">¡Compra y vende artículos únicos!</h2>
        <p class="lead">Bienvenido a Tedzukuri Shop, tu tienda de artículos únicos hechos a mano con el toque personal
            de cada artísta,
            donde podrás comprar y poner a la venta productos artesanales. Escríbenos pulsando el boton que aparece
            debajo de este texto o
            en la pestaña de contacto arriba en la barra de navegación si tienes alguna duda sobre cualquier cuestión,
            estaremos encantados de responder a todas tus cuestiones :) </p>
        <a href="{{ url('/contacto') }}" class="btn btn-primary btn-lg">Quiero más información >></a>
    </header>

    <h3>Echa un vistazo a los últimos productos ↓</h3>
    <!-- <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="{{asset ('img/nessy.jpg')}}" width="500px" height="500px" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="{{asset ('img/cangrejo.jpg') }}" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="{{asset ('img/pingu.jpeg') }}" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div> -->

    <!-- carousel bueno -->
    <div id="div_carousel" class="row align-items-center justify-content-center container w-75">
        <div id="carouselExampleControls" class="carousel slide my-4 flex-center" data-ride="carousel">
            <div class="carousel-inner ">
                <div class="carousel-item active">
                    <img id="imgCarousel" class=" w-100 img-fluid" src="{{asset ('img/pingu.jpeg') }}">
                </div>
                <div class="carousel-item">
                    <img id="imgCarousel" class=" w-100 img-fluid" src="{{asset ('img/cangrejo.jpg') }}">
                </div>
                <div class="carousel-item">
                    <img id="imgCarousel" class=" w-100 img-fluid" src="{{asset ('img/nessy.jpg') }}">
                </div>
            </div>

            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>

    <!-- Carousel con cards
              <div class="container my-4 w-50">

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    Diapositivas 
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="card bg-dark text-white m-width-100">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body">
                            <h5 class="card-title">Card title 1</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="https://i.stack.imgur.com/yyE56.png" alt="..." width="100%">
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="card bg-dark text-white m-width-100">
                <div class="row g-0">
                    <div class="col-md-6">
                        <div class="card-body">
                            <h5 class="card-title">Card title 2</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="https://i.stack.imgur.com/yyE56.png" alt="..." width="100%">
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <div class="card bg-dark text-white m-width-100">
                <div class="row g-0">
                    <div class="col-md-6">
                        <div class="card-body">
                            <h5 class="card-title">Card title 3</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="https://i.stack.imgur.com/yyE56.png" alt="..." width="100%">
                    </div>
                </div>
                </div>
        </div>
    </div>
    <Controles 
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

</div> -->
<h2>Busca los artículos que quieras por categoría, color, título, autor/a...</h2>
<div id="filtros">
            <form id="filtro" class="form-inline">
            <label><strong>Elige tu filtro</strong> </label>
                <select name="searchProduct" class="form-control mr-sm-2" id="exampleFormControlSelect1">
                    <option value="title">Título</option>
                    <option value="price">Precio</option>
                    <option value="color">Color</option>
                    <option value="author">Autor/a</option>
                </select>
                <input name="type" class="form-control mr-sm-1" type="search" placeholder="Buscar por nombre"
                    aria-label="Search">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Filtrar</button>
            </form>

            <form id="filtro" class="form-inline">
            <label><strong>Filtra por categoría</strong></label>
                <select name="searchC" class="form-control mr-sm-2" id="exampleFormControlSelect1">
                    <option value="1">Resin</option>
                    <option value="2">Polymer clay figure</option>
                    <option value="3">Art print</option>
                    <option value="4">Acrylic painting</option>
                    <option value="5">Watercolor painting</option>
                </select>
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Filtrar</button>
            </form>

            <form id="filtro" class="form-inline">
            <label><strong>Elige el precio</strong></label>
            <input name="searchP" class="form-control mr-sm-2" type="search" placeholder="Introduce una cantidad" aria-label="Search">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Filtrar</button>
            </form>

            <form id="filtro" class="form-inline">
            <label><strong>Filtra por precio</strong></label>
                <select name="priceSearch" class="form-control mr-sm-2" id="exampleFormControlSelect1">
                    <option value="asc">Más bajo</option>
                    <option value="desc">Más alto</option>
                </select>
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Filtrar</button>
            </form>
</div><br>

<br>
<h4>✨⭐Productos destacados⭐✨</h4><br>
    <div class="row hidden-md-up">
        @foreach($products as $product)
        @if($product->stock =! 0 )
        <div class="col-lg-3 product_index">
            <div class="card h-100">
                <a href="/products/detail/{{$product->id_product}}">
                    <img class="card-img-top" src="{{URL::asset('img/'.$product->image)}}" width="200px" height="200px"
                        alt="{{ $product->title }}">
                </a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="/products/detail/{{$product->id_product}}">{{ $product->title }}</a>
                    </h4>
                    <h5>{{ $product->price }}€</h5>
                </div>
                <div class="card-footer">
                    <p class="card-text">Autor/a: {{ $product->author }}</p>
                </div>
            </div>
        </div>
        @else
        <p>No hay productos disponibles para esa busqueda</p>
        @endif
        @endforeach
    </div><br>
    <!-- /.row -->
    <span>
        <!-- pagination -->
        {{$products->links()}}
    </span>

</div>
<!-- /.container -->
@endsection