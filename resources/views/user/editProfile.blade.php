@extends('layouts.app')


@section('content')
<?php
        if (!isset($usuario))
        {
            $id = $name=$email=$address=$id_role= "";
            $action = "create";
        }
        else
        {
            
            $id = $usuario->id_user;
            $name = $usuario->name;
            $email = $usuario->email;
            $address = $usuario->address;
            $id_role = $usuario->id_role;
 
            $action = url("/editProfile/update");
        }
    ?>
<div class="container divlogin">
    <div class="row">
        <div class="col-lg-12 text-center">
            <form action="{{$action}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}


                <h1>Modify Profile</h1>
                <div class="form-group">
                    <label>name: </label>
                    <input type="text" class="form-control" name="name" value="{{old('name',$name)}}" readonly>
                </div>
                <div class="form-group">
                    <label>email: </label>
                    <input type="text" class="form-control" name="email" value="{{old('email',$email)}}">
                </div>
                <div class="form-group">
                    <label>address:</label>
                    <input type="text" class="form-control" name="address" value="{{old('address',$address)}}">
                </div>
                <div class="form-group row">
                    <label for="role" class="col-md-4 col-form-label text-md-right">¿Qué vas a hacer en la
                        página?</label>

                    <div class="col-md-6">
                        <select name="id_role" id="id_role">
                            <option value="1" {{old("id_role") == "1" ? "selected":"" }}>Quiero comprar</option>
                            <option value="2" {{old("id_role") == "2" ? "selected":"" }}>Quiero comprar y vender</option>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="id" value="{{$id}}">
                <input type="submit" class="btn btn-success" value="Enviar">
                @if(count($errors) > 0)
                <div class="errors">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>

@endsection