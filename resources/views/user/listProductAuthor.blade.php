@extends('layouts.app')


@section('content')

<h1>Products in data base</h1>

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Description</th>
            <th>Title</th>
            <th>Image</th>
            <th>stock</th>
            <th>price</th>
            <th>size</th>
            <th>color</th>
            <th>Author</th>
            <th>Modify</th>
            <th>Delete</th>
        </tr>
       
    </thead>
    <tbody>
        @foreach($products as $product)
        @if(Auth::user()->id_role == 3)
        <tr>
            <td>{{ $product->id_product }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->title }}</td>
            <td>
                <img class="card-img-top" src="{{URL::asset('img/'.$product->image)}}" width="200px" height="200px" alt="{{ $product->title }}">
            </td>
            <td>{{ $product->stock }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->size }}</td>
            <td>{{ $product->color }}</td>
            <td>{{ $product->author }}</td>
            <td><a href="products/update/{{$product->id_product}}" class="btn btn-primary">UPDATE</a></td>
            <td><a href="products/destroy/{{$product->id_product}}" class="btn btn-primary">X</a></td>
        </tr>
        @elseif(Auth::user()->name == $product->author)
        <tr>
            <td>{{ $product->id_product }}</td>
            <td>{{ $product->description }}</td>
            <td><a href="/products/detail/{{$product->id_product}}">{{ $product->title }}</a></td>
            <td>
                <a href="/products/detail/{{$product->id_product}}">
                    <img class="card-img-top" src="{{URL::asset('img/'.$product->image)}}" width="200px"
                        height="200px" alt="{{ $product->title }}">
                </a>
            </td>
            <td>{{ $product->stock }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->size }}</td>
            <td>{{ $product->color }}</td>
            <td>{{ $product->author }}</td>
            <td><a href="products/update/{{$product->id_product}}" class="btn btn-primary">UPDATE</a></td>
            <td><a href="products/destroy/{{$product->id_product}}" class="btn btn-primary">X</a></td>
        </tr>
        @endif
        @endforeach
    </tbody>
</table>


<a href="products/create" class="btn btn-primary">Añadir producto</a>
<span class="center">
    {{$products->links()}}
</span>


@endsection