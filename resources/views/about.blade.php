@extends('layouts.app')

@section('content')
 <!-- Page Content -->
 <div class="aboutContainer">
     <div class="imgAbout">
         <img src="{{asset ('img/about.jpg') }}" width="400" height="500" alt="aboutMe">
     </div>
     <div>
         <h2>Sobre mi</h2>
         <p>Hola ᵔᴥᵔ

             ¡Bienvenidos a mi tienda! Mi nombre es Fox y nací y crecí en España.

             Me encantan los animales, las plantas y son mis principales inspiraciones que encontrarás en la mayoría de
             mis creaciones. He pasado la mayor parte de mi tiempo cuando era niña averiguando lo que quería hacer
             cuando fuera mayor y el arte es algo que siempre me ha apasionado.

             Disfruto mucho haciendo arte y eso es lo que me gusta hacer. Soy una gran fan de los zorros, y me encanta
             pasar el tiempo haciendo figuritas y animales gordos.

             También me encanta perderme en mi propio espacio, que soy yo y mis pinturas, pinceles, arcilla y mi
             creatividad; es entonces cuando intento crear arte que esté fuera de mi zona de confort. Me encanta hacer
             animales raros y de fantasía. Creo artículos nuevos cada semana 🙂 .

             Hago a mano todo tipo de pequeños regalos y figuritas. Me especializo en artículos de arcilla polimérica.
             Todo está diseñado y hecho por mí. ¡Incluso hago artículos personalizados por encargo! Pongo mi amor y
             cuidado, y mucho esfuerzo en todos y cada uno de los artículos.

             Gracias por visitar mi tienda. Espero que encuentres algo interesante para ti.

             ¡Gracias! 💙</p>
     </div>
 </div>
 @endsection