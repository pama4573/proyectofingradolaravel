@extends('layouts.app')

@section('content')
<!-- Page Content -->
    <div class="detailContainer">
            <div>
                <img class="imgDetail" src="{{URL::asset('img/'.$product->image)}}" style="height:400px"/>
            </div>
            <div>
                <h2 style="min-height:45px;margin:5px 0 5px 0">
                    {{ $product->title }}   
                </h2>
                <p style="min-height:45px;margin:5px 0 5px 0">
                    <strong>Price: </strong>
                    {{ $product->price }}€
                </p>
                <p style="min-height:45px;margin:5px 0 5px 0">
                    <strong>Author: </strong>
                    {{ $product->author }}
                </p>
                <p style="min-height:45px;margin:5px 0 5px 0">
                    <strong>Description: </strong>
                    {{ $product->description }}
                </p>
                <p style="min-height:45px;margin:5px 0 5px 0">
                    <strong>Tamaño: </strong>
                    {{ $product->size }}
                </p>
                <p style="min-height:45px;margin:5px 0 5px 0">
                    <strong>Color: </strong>
                    {{ $product->color }}
                </p>
                <p style="min-height:45px;margin:5px 0 5px 0">
                    <strong>Stock: </strong>
                    @if( $product->stock == 0 )
                        Producto agotado<br>
                    @else
                        Producto disponible <br>
                        Unidades disponibles: {{ $product->stock }} <br>
                        <!-- <button type="button" class="btn btn-primary">Añadir al carrito</button> -->

                        <br>
                        <a class="btn btn-primary" href="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
                            <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                        </svg>
                            Añadir al carrito
                        </a>
                    @endif

                        <a class="btn btn-secondary" href="{{ url('/') }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-shop" viewBox="0 0 16 16">
                            <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z"/>
                        </svg>
                            Volver al catálogo
                        </a>
                    
                </p>
            </div>
        </div>
    </div>
@endsection