@extends('layouts.app')


@section('content')
<?php
        if (!isset($product))
        {
            $id = $description = $title=$image=$stock=$price=$size=$color= "";
            $action = "create";
        }
        else
        {
            $id = $product->id_product;
            $description = $product->description;
            $title = $product->title;
            $image = $product->image;
            $stock = $product->stock;
            $price = $product->price;
            $size = $product->size;
            $color = $product->color;
            $author = $product->author;
            $id_category = $product->id_category;
            $action = url("/products/update");
        }
    ?>

<div class="create">
    <div class="col-sm-8">
        <form action="{{$action}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}

            @if($action == "create")
            <h1 class="text-center">Create product</h1>
            <div class="form-group">
                <label>Description: </label>
                <textarea class="form-control" name="description" value="{{old('description')}}" rows="6" placeholder="Añade una pequeña descripción de tu producto"></textarea>
            </div><br>
            <div class="form-group">
                <label>Title: </label>
                <input type="text" class="form-control" name="title" value="{{old('title')}}" placeholder="Ponle un título a tu creación para que otros usuarios puedan encontrarlo facilmente">
            </div><br>
            <div class="form-group">
                <label>Imagen:</label>
                <input type="file" class="form-control" name="file">
            </div><br>
            <div class="form-group">
                <label>Stock: </label>
                <input type="text" class="form-control" name="stock" value="{{old('stock')}}" placeholder="Indica que número de unidades deseas poner a la venta">
            </div><br>
            <div class="form-group">
                <label>Price: </label>
                <input type="text" class="form-control" name="price" value="{{old('price')}}" placeholder="Indica el precio de tu producto">
            </div><br>
            <div class="form-group">
                <label>Size: </label>
                <input type="text" class="form-control" name="size" value="{{old('size')}}" placeholder="Indica las medidas de tu creación (5x5cm, 7x3cm, etc.)">
            </div><br>
            <div class="form-group">
                <label>Color: </label>
                <input type="text" class="form-control" name="color" value="{{old('color')}}" placeholder="Escribe que colores tiene tu creación para que otros usuarios puedan encontrarlo por los colores más destacados">
            </div><br>
            <div class="form-group ">
                <label>Author: </label>
                <input type="text" class="form-control" name="author" value="{{ Auth::user()->name }}" readonly>
            </div><br>

            <div class="col-12">
                <div class="form-group">
                    <label>Category: </label>
                    <select class="form-control" name="id_category">
                        <option value="1" {{old("id_category") == "1" ? "selected":"" }}>Resin</option>
                        <option value="2" {{old("id_category") == "2" ? "selected":"" }}>Polymer clay figure</option>
                        <option value="3" {{old("id_category") == "3" ? "selected":"" }}>Art print</option>
                        <option value="4" {{old("id_category") == "4" ? "selected":"" }}>Acrylic painting</option>
                        <option value="5" {{old("id_category") == "5" ? "selected":"" }}>Watercolor painting</option>
                    </select>
                </div>
            </div><br>
            <input type="submit" class="btn btn-success" value="Añadir producto">
            @else
            <h1 class="text-center">Update product</h1>
            <div class="form-group">
                <label>Description: </label>
                <input type="text" class="form-control" name="description" value="{{old('description',$description)}}" rows="6" >
            </div><br>
            <div class="form-group">
                <label>Title: </label>
                <input type="text" class="form-control" name="title" value="{{old('title',$title)}}">
            </div><br>
            <div class="form-group">
                <label>Imagen:</label>
                <input type="file" class="form-control" name="file" value="{{old('file',$image)}}" required>
            </div><br>
            <div class="form-group">
                <label>Stock: </label>
                <input type="text" class="form-control" name="stock" value="{{old('stock',$stock)}}">
            </div><br>
            <div class="form-group">
                <label>Price: </label>
                <input type="text" class="form-control" name="price" value="{{old('price',$price)}}">
            </div><br>
            <div class="form-group">
                <label>Size: </label>
                <input type="text" class="form-control" name="size" value="{{old('size',$size )}}">
            </div><br>
            <div class="form-group">
                <label>Color: </label>
                <input type="text" class="form-control" name="color" value="{{old('color',$color)}}">
            </div><br>
            <div class="form-group ">
                <label>Author: </label>
                <input type="text" class="form-control" name="author" value="{{old('author',$author) }}" readonly>
            </div><br>


            <div class="col-12">
                <div class="form-group">
                    <select class="form-control" name="id_category">
                        <option value="1" {{old("id_category") == "1" ? "selected":"" }}>Resin</option>
                        <option value="2" {{old("id_category") == "2" ? "selected":"" }}>Polymer clay figure</option>
                        <option value="3" {{old("id_category") == "3" ? "selected":"" }}>Art print</option>
                        <option value="4" {{old("id_category") == "4" ? "selected":"" }}>Acrylic painting</option>
                        <option value="5" {{old("id_category") == "5" ? "selected":"" }}>Watercolor painting
                        </option>
                    </select>
                </div>
            </div><br>
            <input type="submit" class="btn btn-success" value="Modificar producto">
            @endif


            <!--<div class="form-group">
                <label>Description: </label>
                <input type="text" class="form-control" name="description"
                    value="{{old('description',$description)}}">
            </div>
            <div class="form-group">
                <label>Title: </label>
                <input type="text" class="form-control" name="title" value="{{old('title',$title)}}">
            </div>
            <div class="form-group">
                <label>Imagen:</label>
                <input type="file" class="form-control" name="file">
            </div>
            <div class="form-group">
                <label>Stock: </label>
                <input type="text" class="form-control" name="stock" value="{{old('stock',$stock)}}">
            </div>
            <div class="form-group">
                <label>Price: </label>
                <input type="text" class="form-control" name="price" value="{{old('price',$price)}}">
            </div>
            <div class="form-group">
                <label>Size: </label>
                <input type="text" class="form-control" name="size" value="{{old('size',$size )}}">
            </div>
            <div class="form-group">
                <label>Color: </label>
                <input type="text" class="form-control" name="color" value="{{old('color',$color)}}">
            </div>
            <div class="form-group ">
                <label>Author: </label>
                <input type="text" class="form-control" name="author" value="{{Auth::user()->name  }}" readonly>
            </div>

            <div class="form-group ">
                <label>Category: </label>
                <input type="text" class="form-control" name="id_category" value="{{old('id_category')}}">
            </div>-->
            <input type="hidden" name="id" value="{{$id}}">
            <a class="btn btn-secondary" href="{{ url('/') }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-shop" viewBox="0 0 16 16">
                    <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z"/>
                </svg>
                    Volver al catálogo
            </a>
            @if(count($errors) > 0)
            <div class="errors">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </form>
    </div>
</div>
@endsection