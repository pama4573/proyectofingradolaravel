@extends('layouts.app')

@section('content')
<h1>Administrar usuarios</h1>
<div class="users">
    <table class="table table-striped ">
        <thead>
            <tr>
                <th id="colTitle">ID</th>
                <th id="colTitle">NOMBRE</th>
                <th id="colTitle">EMAIL</th>
                <th id="colTitle">DIRECCIÓN</th>
                <th id="colTitle">FECHA DE CREACIÓN</th>
                <th id="colTitle">FECHA DE MODIFICACIÓN</th>
                <th id="colTitle">ROL</th>
                <th id="colTitle">MODIFICAR USUARIO</th>
                <th id="colTitle">ELIMINAR USUARIO</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $usuario)
            <tr>
                <td>{{$usuario -> id_user}}</td>
                <td>{{$usuario -> name}}</td>
                <td>{{$usuario -> email}}</td>
                <td>{{$usuario -> address}}</td>
                <td>{{$usuario -> created_at}}</td>
                <td>{{$usuario -> updated_at}}</td>
                <td>{{ $roles[$usuario->id_role - 1]->name}}</td>
                <td><a href="usersCRUD/update/{{$usuario->id_user}}" class="btn btn-primary">UPDATE</a></td>
                <td>
                    <form action="{{ route('usersCRUD.destroy', $usuario->id_user) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">X</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection