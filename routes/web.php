<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Route::get('/', function () {
    return view('index');
}); */

Route::get('/', 'ProductController@index');

Route::get('/about', function () {
    return view('about');
});
Route::get('/contacto', function () {
    return view('contacto');
});
Route::get('/pago', function () {
    return view('welcome');
});

// route for view/blade file
Route::get('/paywithpaypal', array('as' => 'paywithpaypal','uses' => 'PaymentController@payWithPaypal',));
// route for post request
Route::post('paypal', array('as' => 'paypal','uses' => 'PaymentController@postPaymentWithpaypal',));
// route for check status responce
Route::get('paypal', array('as' => 'status','uses' => 'PaymentController@getPaymentStatus',));


/*VIEWS ADMIN*/
Route::get('/usersCRUD', 'AdminController@listAllUsers')->name('listAllUsers');
Route::get('/usersCRUD/update/{id}', 'AdminController@updateForm');
Route::post('/usersCRUD/update', 'AdminController@update');
Route::delete('/usersCRUD/{id}', 'AdminController@destroy')->name('usersCRUD.destroy');

Route::get('/editProfile/update/{id}', 'AdminController@updateProfile')->name('update.id');
Route::post('/editProfile/update', 'AdminController@updateP');

/*VIEWS PRODUCT */
Route::get('/listProductAuthor', 'ProductController@listAllProducts')->name('user.listProductAuthor');
Route::get('products/create', 'ProductController@create');
Route::post('products/create', 'ProductController@store');
Route::get('products/update/{id}', 'ProductController@updateForm');
Route::post('products/update', 'ProductController@update');
Route::get('products/destroy/{id}', 'ProductController@destroy');
Route::get('products/detail/{id}', 'ProductController@show')->name('products.detail');


Auth::routes();


/*Route::get('/home', 'HomeController@index')->name('home');*/
